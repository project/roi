<?php

/**
 * @file
 * Template for Compare Sheet PDF.
 */

/**
 * @file This template will show the user the label preview.
 *
 * Available variables:
 * - $comparelist: CompareList products.
 */

$roivalues = !empty($roivalues) ? $roivalues : null;

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">
	<html lang="en" dir="ltr" version="HTML+RDFa 1.1" xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
		<head profile="http://www.w3.org/1999/xhtml/vocab">
  			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  			<style type="text/css">
    			<?php print $inline_css; ?>
  			</style>
  			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		</head>
		
		<body>
			<?php print roi_calculator_html($roivalues); ?>
		</body>
	</html>